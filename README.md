# python-space
## Intro
**python-space** is a python project with scripts and notebooks that helps in calculation and ploting of data useful for a satellite mission. Includes:
* TLE availability for satellite and its epoch value,
* satellite position (based on TLE),
* satellite visibility,
* satellite-Sun position,
* magnetic induction vector for satellite position (based on IGRF)

![Alt Text](/SENTINEL-2A_2020-11-05_00-00-00_UTC_2020-11-05_23-59-50_UTC.gif)

## Content:
* [notebooks](/notebooks) - jupyter notebooks
    * [sat-tle-search](/notebooks/sat-tle-search.ipynb) - script that allows to search agains available data in CelesTrak database
    * [sat-position](/notebooks/sat-position.ipynb) - script that allows to calculate and plot satellites' positions in time based on TLE data
    * [sat-mission-data](/notebooks/sat-mission-data.ipynb) - script that allows to calculate and plot data needed to plan/perform satellite's on-orbit mission
* [pyspace](/pyspace) - python script that calculates/plots data based on methods presented in [sat-mission-data](/notebooks/sat-mission-data.ipynb) notebook
* [python-space.yml](/python-space.yml) - Anaconda environment

## How to run:
0. Requirements:
    - Anaconda
    - python IDE (e.g. Pycharm, VS Code, Spyder)
1. Install python-space.yml Anaconda environment.
2. Activa python-space Anaconda environment.
3. Open folder [notebooks](/notebooks) in JupyterLab and run selected notebook.
4. Run [pyspace.py](/pyspace/pyspace.py) with following parammeters:
    - -h / --help - show help message and exit
    - -s / --satellite - Satellite (e.g. -s "ISS (ZARYA)")
    - -gs / --groundstations - List of ground controls (e.g. -gs "KRK-OBS;50.0539N;19.8245E" "KSC-LCC;28.585N;-80.649E")
    - -ts / --timestart - Start UTC time (e.g. -ts "2021-02-21 00:00")
    - -te / --timeend - End UTC time (e.g. -te "2021-02-22 00:00")

## Useful links:
- [CelesTrack](https://celestrak.com/) - TLE database
- [Skyfield](https://rhodesmill.org/skyfield/) - astronomy package for Python
- [pyIGRF](https://github.com/zzyztyy/pyIGRF) - IGRF package for Python