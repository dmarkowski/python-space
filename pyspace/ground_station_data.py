from datetime import datetime
from typing import List, Tuple

from skyfield.toposlib import Topos
from skyfield.units import Angle, Distance


class GroundStationData:
    events: List[Tuple[datetime, str]]
    altaz_positions: List[Tuple[Angle, Angle, Distance]]
    is_visible: List[bool]

    def __init__(self, data: str):
        details = data.split(";")
        self.name = details[0]
        self.topos = Topos(details[1], details[2])