from typing import List, Tuple
from skyfield.positionlib import ICRF
from skyfield.sgp4lib import EarthSatellite
from skyfield.toposlib import Topos
from skyfield.units import Angle, Distance

from timescale import ts
from datetime import datetime


class SatelliteWrapper:
    def __init__(self, satellite: EarthSatellite, tle: List[str]):
        self._sf_satellite = satellite
        self.name = satellite.name
        self.tle = tle
        self.tle_days = ts.now() - satellite.epoch
        self.number = int(tle[1][2:7])
        year_dec = int(tle[1][9:11])
        self.launch_year = year_dec+1900 if year_dec > 56 else year_dec+2000
        self.tle_epoch_year = 2000 + int(tle[1][18:20])
        self.tle_epoch_day = float(tle[1][20:32])
        self.avg_rev_per_day = float(tle[2][52:63])
        self.rev_total = int(tle[2][63:-1])

    def at(self, time: datetime) -> ICRF:
        t = ts.utc(time)
        return self._sf_satellite.at(t)

    def find_events(self, ground_station: Topos, time: List[datetime], altitude_degrees: float) -> List[Tuple[datetime, str]]:
        t_s = ts.utc(time[0])
        t_e = ts.utc(time[-1])
        event_times, events = self._sf_satellite.find_events(ground_station, t_s, t_e, altitude_degrees)
        events_name = [f'{self.name} rose above {altitude_degrees}.',
                       f'{self.name} culminated.',
                       f'{self.name} fell below {altitude_degrees}.']
        return [(t.utc_datetime(), events_name[e]) for t, e in zip(event_times, events)]

    def relative_altaz(self, ground_station: Topos, time: datetime) -> Tuple[Angle, Angle, Distance]:
        """Compute (alt, az, distance) relative to the ground station's horizon"""
        t = ts.utc(time)
        return (self._sf_satellite - ground_station).at(t).altaz()

    def __str__(self) -> str:
        return (f'Satellite {self.name}\n' +
                f'number: {self.number}\n' +
                f'launch year: {self.launch_year}\n' +
                f'avg. revolutions per data: {self.avg_rev_per_day}\n' +
                f'total revolutions since launch: {self.rev_total}\n' +
                f'tle epoch year: {self.tle_epoch_year} day: {self.tle_epoch_day}\n' +
                f'TLE {self.tle_days:.4f} days away from epoch\n{self.tle[0]}\n{self.tle[1]}\n{self.tle[2]}\n')
