from datetime import datetime
from typing import List, Tuple

from skyfield.positionlib import Geocentric
from skyfield.toposlib import Topos
from skyfield.units import Distance

from satellite_wrapper import SatelliteWrapper
from ground_station_data import GroundStationData


class SatelliteData:
    satellite: SatelliteWrapper
    time: List[datetime]
    positions: List[Geocentric]
    icrf_positions: List[Distance]
    topos_positions: List[Topos]
    events: List[Tuple[datetime, str]]
    is_sunlist: List[bool]
    ground_stations_data: List[GroundStationData]
    igrf_mag_vectors: List[List[float]]