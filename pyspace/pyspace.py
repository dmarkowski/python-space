import argparse
from datetime import datetime, timedelta, timezone
from timescale import time_generator
from satellite_observer import SatelliteObserver
from satellite_plotter import SatellitePlotter
from constant import DATETIME_INPUT_FORMAT, DATETIME_FORMAT

parser = argparse.ArgumentParser(description='Run pyspace.')
parser.add_argument('-s', '--satellite', type=str, required=True,
                    help='Satellite (e.g. -s "ISS (ZARYA)")')
parser.add_argument('-gs', '--groundstations', default=[], nargs='*', type=str,
                    help='List of ground controls (e.g. -gs "KRK-OBS;50.0539N;19.8245E" "KSC-LCC;28.585N;-80.649E")')
parser.add_argument('-ts', '--timestart', type=str, required=True,
                    help=f'Start UTC time (e.g. -ts "2021-02-21 00:00")')
parser.add_argument('-te', '--timeend', type=str, required=True,
                    help=f'End UTC time (e.g. -te "2021-02-22 00:00")')

args = parser.parse_args()
sat_name = args.satellite
ground_stations = args.groundstations
time_start = datetime.strptime(args.timestart, DATETIME_INPUT_FORMAT).replace(tzinfo=timezone.utc)
time_end = datetime.strptime(args.timeend, DATETIME_INPUT_FORMAT).replace(tzinfo=timezone.utc)
time_delta = timedelta(seconds=30)
time_list = time_generator(time_start, time_end, time_delta)

sat_plotter = SatellitePlotter()

satellite_data = SatelliteObserver()\
    .init_observation(sat_name, ground_stations, time_list)\
    .compute_geocentric_icrf_positions()\
    .compute_topos_positions()\
    .compute_is_sunlit()\
    .compute_ground_station_data()\
    .compute_igrf_mag_data()\
    .get_data()

print(satellite_data.satellite)
for gs_data in satellite_data.ground_stations_data:
     print(f'{gs_data.name} events:')
     for t, e in gs_data.events:
        print(f'{t.strftime(DATETIME_FORMAT)}: {e}')

sat_plotter.plot(satellite_data)
