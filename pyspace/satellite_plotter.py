from satellite_observer import SatelliteObserver
from typing import List, Tuple

import numpy as np

from ground_station_data import GroundStationData
from satellite_data import SatelliteData
from datetime import datetime
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from matplotlib.widgets import Slider, Button, CheckButtons
from constant import DATETIME_FORMAT, DATETIME_XLABEL_FORMAT
import matplotlib.dates as mdates


class SatellitePlotter:
    """A class that provides methods for plotting data related to satellite."""

    @staticmethod
    def plot(satellite_data: SatelliteData):
        SatellitePlotter._plot_icrf_position(satellite_data)
        SatellitePlotter._plot_topos_position(satellite_data)
        for gs_data in satellite_data.ground_stations_data:
            SatellitePlotter._plot_altaz_position(gs_data, satellite_data.satellite.name, satellite_data.time)
        SatellitePlotter._plot_timeline(satellite_data)
        SatellitePlotter._plot_igrf_mag_data(satellite_data)
        SatellitePlotter._plot_earth(satellite_data)
        plt.show()

    @staticmethod
    def _plot_icrf_position(satellite_data: SatelliteData):
        x = [p.km[0] for p in satellite_data.icrf_positions]
        y = [p.km[1] for p in satellite_data.icrf_positions]
        z = [p.km[2] for p in satellite_data.icrf_positions]
        fig = plt.figure()
        ax1 = fig.add_subplot(311)
        ax1.set_ylabel('x[km]')
        ax1.set_xlabel('time')
        ax1.set_title(f'Geocentric ICRF position {satellite_data.satellite.name}\n{satellite_data.time[0].strftime(DATETIME_FORMAT)} - {satellite_data.time[-1].strftime(DATETIME_FORMAT)}')
        ax1.plot(satellite_data.time, x)
        ax2 = fig.add_subplot(312)
        ax2.set_ylabel('y[km]')
        ax2.set_xlabel('time')
        ax2.plot(satellite_data.time, y)
        ax3 = fig.add_subplot(313)
        ax3.set_ylabel('z[km]')
        ax3.set_xlabel('time')
        ax3.plot(satellite_data.time, z)
        ax1.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax2.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax3.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax1.grid()
        ax2.grid()
        ax3.grid()

    @staticmethod
    def _plot_igrf_mag_data(satellite_data: SatelliteData):
        x = [v[0] for v in satellite_data.igrf_mag_vectors]
        y = [v[1] for v in satellite_data.igrf_mag_vectors]
        z = [v[2] for v in satellite_data.igrf_mag_vectors]
        fig = plt.figure()
        ax1 = fig.add_subplot(311)
        ax1.set_ylabel('x(North)[nT]')
        ax1.set_xlabel('time')
        ax1.set_title(f'IGRF magnetic induction vector {satellite_data.satellite.name}\n{satellite_data.time[0].strftime(DATETIME_FORMAT)} - {satellite_data.time[-1].strftime(DATETIME_FORMAT)}')
        ax1.plot(satellite_data.time, x)
        ax2 = fig.add_subplot(312)
        ax2.set_ylabel('y(East)[nT]')
        ax2.set_xlabel('time')
        ax2.plot(satellite_data.time, y)
        ax3 = fig.add_subplot(313)
        ax3.set_ylabel('z(Down)[nT]')
        ax3.set_xlabel('time')
        ax3.plot(satellite_data.time, z)
        ax1.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax2.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax3.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax1.grid()
        ax2.grid()
        ax3.grid()

    @staticmethod
    def _plot_topos_position(satellite_data: SatelliteData):
        latitude = [p.latitude.degrees for p in satellite_data.topos_positions]
        longitude = [p.longitude.degrees for p in satellite_data.topos_positions]
        elevation = [p.elevation.km for p in satellite_data.topos_positions]
        fig = plt.figure()
        ax1 = fig.add_subplot(311)
        ax1.set_ylabel('longitude[°]')
        ax1.set_xlabel('time')
        ax1.set_title(f'Topos position {satellite_data.satellite.name}\n{satellite_data.time[0].strftime(DATETIME_FORMAT)} - {satellite_data.time[-1].strftime(DATETIME_FORMAT)}')
        ax1.plot(satellite_data.time, longitude)
        ax2 = fig.add_subplot(312)
        ax2.set_ylabel('latitude[°]')
        ax2.set_xlabel('time')
        ax2.plot(satellite_data.time, latitude)
        ax3 = fig.add_subplot(313)
        ax3.set_ylabel('elevation[km]')
        ax3.set_xlabel('time')
        ax3.plot(satellite_data.time, elevation)
        ax1.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax2.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax3.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax1.grid()
        ax2.grid()
        ax3.grid()

    @staticmethod
    def _plot_altaz_position(ground_satation_data: GroundStationData, satellite_name: str, time:List[datetime]):
        alt = [p[0].degrees for p in ground_satation_data.altaz_positions]
        az = [p[1].degrees for p in ground_satation_data.altaz_positions]
        distance = [p[2].km for p in ground_satation_data.altaz_positions]

        fig = plt.figure()
        ax1 = fig.add_subplot(311)
        ax1.set_ylabel('altitude[°]')
        ax1.set_xlabel('time')
        ax1.set_title(f'Topocentric altaz position {ground_satation_data.name} {satellite_name}\n{time[0].strftime(DATETIME_FORMAT)} - {time[-1].strftime(DATETIME_FORMAT)}')
        ax1.plot(time, np.zeros(np.size(time)), color='black', linestyle='--', marker='')
        ax1.plot(time, alt)
        ax2 = fig.add_subplot(312)
        ax2.set_ylabel('azimuth[°]')
        ax2.set_xlabel('time')
        ax2.plot(time, az)
        ax3 = fig.add_subplot(313)
        ax3.set_ylabel('distance[km]')
        ax3.set_xlabel('time')
        ax3.plot(time, distance)
        ax1.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax2.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax3.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax1.grid()
        ax2.grid()
        ax3.grid()

    @staticmethod
    def _plot_timeline(satellite_data: SatelliteData):
        time = satellite_data.time
        is_sunlit = satellite_data.is_sunlist

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_ylabel('State [True/False]')
        ax1.set_xlabel('time')
        ax1.set_title( f'Timeline for {satellite_data.satellite.name}\n{time[0].strftime(DATETIME_FORMAT)} - {time[-1].strftime(DATETIME_FORMAT)}')
        ax1.plot(time, is_sunlit, color='orange', label=f'{satellite_data.satellite.name} is in sun light')
        for gs_data in satellite_data.ground_stations_data:
            ax1.plot(time, gs_data.is_visible, label=f'{gs_data.name}: {satellite_data.satellite.name} is visible')
        ax1.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_XLABEL_FORMAT))
        ax1.legend()

    @staticmethod
    def _plot_earth(satellite_data: SatelliteData):
        fig_earth = plt.figure()
        plt.title(f'Position {satellite_data.satellite.name}\n{satellite_data.time[0].strftime(DATETIME_FORMAT)} - {satellite_data.time[-1].strftime(DATETIME_FORMAT)}')
        axtime = plt.axes([0.17, 0.01, 0.3, 0.04])
        axprev = plt.axes([0.55, 0.01, 0.1, 0.04])
        axnext = plt.axes([0.67, 0.01, 0.1, 0.04])
        axcheck = plt.axes([0.79, 0.01, 0.11, 0.04])
        bnext = Button(axnext, 'Next', hovercolor='0.975')
        bprev = Button(axprev, 'Prev', hovercolor='0.975')
        stime = Slider(axtime, 'Time', 0, len(satellite_data.time), valinit=0)
        bdetails = CheckButtons(axcheck, ['Details'], [False])
        ax = fig_earth.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
        ax.stock_img()
        details_text = ax.text(0.01, 0.99, '', transform=ax.transAxes, fontsize=8,
                          verticalalignment='top', bbox=dict(boxstyle='round', facecolor='white', alpha=0.8))
        details_text.set_visible(False)
        trajectory_plot, = ax.plot([], [], color='gray', linestyle='', marker='.', markersize=1,
                               transform=ccrs.PlateCarree())
        current_position_plot, = ax.plot([], [], color='red', linestyle='', marker='*',
                                  transform=ccrs.PlateCarree())
        current_label_text = ax.text(0, 0, '', color='red')

        class SatDataCallback(object):

            def __init__(self, sat_data: SatelliteData):
                self.time = sat_data.time
                self.satellite = sat_data.satellite
                self.latitude = [p.latitude.degrees for p in sat_data.topos_positions]
                self.longitude = [p.longitude.degrees for p in sat_data.topos_positions]

            def update_position(self, val):
                i = int(stime.val)
                if i < len(self.time):
                    current_position_plot.set_ydata(self.latitude[i])
                    current_position_plot.set_xdata(self.longitude[i])
                    current_label_text.set_position((self.longitude[i], self.latitude[i]))
                    current_label_text.set_text(
                        f'{self.satellite.name}\n{self.longitude[i]:.2f}°E, {self.latitude[i]:.2f}°N\n{self.time[i].strftime(DATETIME_FORMAT)}')
                    fig_earth.canvas.draw_idle()

            def next_position(self, event):
                val = stime.val + 1
                if (val < len(self.time)):
                    stime.set_val(val)

            def prev_position(self, event):
                val = stime.val - 1
                if val > -1:
                    stime.set_val(val)

            def toggle_details(self, label):
                details_text.set_visible(not details_text.get_visible())
                current_label_text.set_visible(not details_text.get_visible())
                fig_earth.canvas.draw_idle()

            def plot_init(self):
                trajectory_plot.set_ydata(self.latitude)
                trajectory_plot.set_xdata(self.longitude)
                details_text.set_text(str(self.satellite))
                stime.set_val(0)

        callback = SatDataCallback(satellite_data)
        bnext.on_clicked(callback.next_position)
        bprev.on_clicked(callback.prev_position)
        stime.on_changed(callback.update_position)
        bdetails.on_clicked(callback.toggle_details)
        callback.plot_init()
        plt.show()