from typing import List, Tuple

from skyfield.api import load
from skyfield.positionlib import Geocentric
from skyfield.toposlib import Topos
from skyfield.units import Distance, Angle

import pyIGRF

from ground_station_data import GroundStationData
from satellite_data import SatelliteData
from constant import ACTIVE_SATS_TLE_URL, ACTIVE_SATS_TLE_FILE, EVENT_THRESHOLD
from satellite_wrapper import SatelliteWrapper
from datetime import datetime, timezone


class SatelliteObserver:
    """A class that provides methods for computing data related to satellites."""

    def __init__(self):
        sf_satellites = load.tle_file(ACTIVE_SATS_TLE_URL, reload=False)
        self._sf_satellites_dict = {sat.name: sat for sat in sf_satellites}
        self._planets = load('de421.bsp')


    def init_observation(self, sat_name: str, ground_stations: List[str], time_list: List[datetime]):
        self._data = SatelliteData()
        self._data.satellite = SatelliteWrapper(self._sf_satellites_dict[sat_name], self._get_satellite_tle(sat_name))
        self._data.ground_stations_data = [GroundStationData(gs) for gs in ground_stations]
        self._data.time = time_list
        self._data.positions = [self._data.satellite.at(t) for t in time_list]
        return self

    @staticmethod
    def _get_satellite_tle(sat_name: str) -> List[str]:
        sat_tle = ['', '', '']
        with open(ACTIVE_SATS_TLE_FILE) as tle_file:
            for num, line in enumerate(tle_file):
                if sat_name in line:
                    sat_tle[0] = line.strip()
                    sat_tle[1] = tle_file.readline(num + 1).strip()
                    sat_tle[2] = tle_file.readline(num + 2).strip()
        return sat_tle

    def compute_geocentric_icrf_positions(self):
        self._data.icrf_positions = [p.position for p in self._data.positions]
        return self

    def compute_topos_positions(self):
        self._data.topos_positions = [p.subpoint() for p in self._data.positions]
        return self

    def compute_is_sunlit(self):
        self._data.is_sunlist = [p.is_sunlit(self._planets) for p in self._data.positions]
        return self

    def compute_ground_station_data(self):
        for gs_data in self._data.ground_stations_data:
            gs_data.events = self._get_events(gs_data.topos)
            gs_data.altaz_positions = self._get_altaz_positions(gs_data.topos)
            gs_data.is_visible = self._get_is_visible(gs_data.altaz_positions)
        return self

    def compute_igrf_mag_data(self):
        self._data.igrf_mag_vectors = [self._calculate_igrf(t,p) for (t,p) in zip(self._data.time, self._data.positions)]
        return self

    def get_data(self):
        return self._data

    def _get_events(self, ground_station: Topos) -> List[Tuple[datetime, str]]:
        return self._data.satellite.find_events(ground_station, self._data.time, EVENT_THRESHOLD)

    def _get_altaz_positions(self, ground_station: Topos) -> List[Tuple[Angle, Angle, Distance]]:
        return [self._data.satellite.relative_altaz(ground_station, t) for t in self._data.time]

    @staticmethod
    def _get_is_visible(altaz_positions: List[Tuple[Angle, Angle, Distance]]) -> List[bool]:
        return [p[0].degrees > 0.0 for p in altaz_positions]

    @staticmethod
    def _datetime2yearfloat(date: datetime):
        ms_pass = (date - datetime(year=date.year, month=1, day=1, hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone.utc))
        ms_total = (datetime(year=date.year + 1, month=1, day=1, hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone.utc) 
                    - datetime(year=date.year, month=1, day=1, hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone.utc))
        return date.year + ms_pass / ms_total

    def _calculate_igrf(self, date: datetime, position: Geocentric) -> List[float]:
        """ return
            igrf_x is north component
            igrf_y is east component
            igrf_z is vertical component (+ve down)
            igrf_f is total intensity
        """
        subpoint = position.subpoint()
        elevation = subpoint.elevation.km
        latitude = subpoint.latitude.degrees
        longitude = subpoint.longitude.degrees
        year_float = self._datetime2yearfloat(date)
        d, i, h, igrf_x, igrf_y, igrf_z, igrf_f = pyIGRF.igrf_value(alt=elevation, lat=latitude, lon=longitude, year=year_float)
        return igrf_x, igrf_y, igrf_z, igrf_f