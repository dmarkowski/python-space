ACTIVE_SATS_TLE_URL = 'http://celestrak.com/NORAD/elements/active.txt'
ACTIVE_SATS_TLE_FILE = 'active.txt'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S%Z'
DATETIME_XLABEL_FORMAT = '%H:%M'
DATETIME_INPUT_FORMAT = '%Y-%m-%d %H:%M'
EVENT_THRESHOLD = 10.0