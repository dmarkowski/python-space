from skyfield import api as sf
from datetime import datetime, timedelta

ts = sf.load.timescale()


def time_generator(start: datetime, end: datetime, delta: timedelta):
    total_seconds = (end - start).total_seconds()
    period_seconds = delta.total_seconds()
    n = int(total_seconds/period_seconds)+2
    time_list = [(start + timedelta(seconds=i*period_seconds)) for i in range(n)]
    return time_list
